CONSOLE = bin/console
PHP = php
PHPUNIT = vendor/bin/phpunit --testdox --colors tests
DOCKER = @docker-compose exec symfony-workflow-engine

start:
	@docker-compose -f docker-compose.yml up -d

restart:
	make stop
	make start

stop:
	@docker-compose stop

run:
	$(PHP) -S 127.0.0.1:8080 -t public

test:
	make start
	$(DOCKER)  $(PHPUNIT) tests

bash:
	$(DOCKER) bash

translation:
	$(PHP) $(CONSOLE) translation:update --dump-messages --force en
	$(PHP) $(CONSOLE) translation:update --dump-messages --force hu

check:
	make start
	$(PHP) $(CONSOLE) cache:clear
	$(DOCKER) $(PHP) $(CONSOLE) lint:container
	$(DOCKER) $(PHP) $(CONSOLE) lint:yaml .
	$(DOCKER) $(PHP) $(CONSOLE) lint:xliff translations
	$(DOCKER) $(PHP) $(CONSOLE) lint:twig templates
	$(DOCKER) $(PHP) $(CONSOLE) doctrine:schema:validate

migration:
	$(DOCKER) $(PHP) $(CONSOLE) make:migration
	$(DOCKER) $(PHP) $(CONSOLE) doctrine:migrations:migrate

graph:
	$(DOCKER) $(PHP) $(CONSOLE) workflow:dump blog_publishing | dot -Tpng -o graph.png

load:
	$(DOCKER) $(PHP) $(CONSOLE) doctrine:fixtures:load --no-interaction --purge-with-truncate