<?php

namespace App\Behavior;

use Doctrine\ORM\Mapping as ORM;

trait Workflowable
{
    /**
     * The configured marking store property must be declared
     *
     * @ORM\Column(type="string", length=15, nullable=true)
     *
     * @var string
     */
    private ?string $currentPlace = null;

    // Getter/setter methods must exist for property access by the marking store
    public function getCurrentPlace(): ?string
    {
        return $this->currentPlace;
    }

    public function setCurrentPlace(string $currentPlace, array $context = []): self
    {
        $this->currentPlace = $currentPlace;
        return $this;
    }
}
