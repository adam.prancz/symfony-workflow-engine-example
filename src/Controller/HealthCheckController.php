<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HealthCheckController
{
    /**
     * @Route("/health-check/ping", name="health_check_ping_action")
     */
    public function ping(): Response
    {
        return new Response(
            'OK'
        );
    }
}
