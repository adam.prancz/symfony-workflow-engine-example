<?php

namespace App\DataFixtures;

use App\Entity\BlogPost;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Exception;
use Symfony\Component\Workflow\Registry;

class BlogPostFixtures extends Fixture
{
    private ?Registry $workflowRegistry;

    public function __construct(Registry $workflowRegistry)
    {
        $this->workflowRegistry = $workflowRegistry;
    }

    /**
     * @throws Exception
     */
    public function load(ObjectManager $manager): void
    {
        for ($i = 0; $i < 100; $i++) {
            $blogPost = (new BlogPost())
                ->setTitle('Test title' . $i)
                ->setContent('Test content' . $i);

            $workflow = $this->workflowRegistry->get($blogPost);
            $places = array_keys($workflow->getDefinition()->getPlaces());

            $blogPost->setCurrentPlace($places[random_int(0, count($places)-1)]);

            $manager->persist($blogPost);
        }

        $manager->flush();
    }
}
