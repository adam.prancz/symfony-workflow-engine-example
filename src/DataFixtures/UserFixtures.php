<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    public const ADMIN_USER_EMAIL = 'test@test.hu';
    public const USER_EMAIL = 'test1@test.hu';

    private ?UserPasswordEncoderInterface $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $roles = [
            'ROLE_ADMIN',
            'ROLE_USER',
        ];

        $user = (new User())
            ->setEmail(self::ADMIN_USER_EMAIL)
            ->setPassword('test')
            ->setRoles($roles);

        $user->setPassword($this->passwordEncoder->encodePassword($user, 'the_new_password'));

        $manager->persist($user);

        $user1 = (new User())
            ->setEmail(self::USER_EMAIL)
            ->setPassword('test')
            ->setRoles([$roles[1]]);

        $user1->setPassword($this->passwordEncoder->encodePassword($user, 'the_new_password'));

        $manager->persist($user1);

        $manager->flush();
    }
}
