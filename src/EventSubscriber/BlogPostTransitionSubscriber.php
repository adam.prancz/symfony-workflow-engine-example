<?php

namespace App\EventSubscriber;

use App\Workflow\Transitions\BlogPostTransitions;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Workflow\Event\GuardEvent;
use Symfony\Component\Workflow\TransitionBlocker;

class BlogPostTransitionSubscriber implements EventSubscriberInterface
{
    private ?LoggerInterface $logger;

    private TokenStorageInterface $tokenStorage;

    public function __construct(LoggerInterface $logger, TokenStorageInterface $tokenStorage)
    {
        $this->logger = $logger;
        $this->tokenStorage = $tokenStorage;
    }

    public function guardPublish(GuardEvent $event): void
    {
        $eventTransition = $event->getTransition();
        $roles = $event->getMetadata('roles', $eventTransition);

        $this->logger->info(sprintf(
            'Guard activated when blog post (id: "%s") performed transition "%s" from "%s" to "%s"',
            $event->getSubject()->getId(),
            $event->getTransition() ? $event->getTransition()->getName() : null,
            implode(', ', array_keys($event->getMarking()->getPlaces())),
            implode(', ', ($event->getTransition() ? $event->getTransition()->getTos() : null))
        ));

        $user = null;
        $token = $this->tokenStorage->getToken();
        if ($token instanceof TokenInterface) {
            $user = $token->getUser();
            if ($user instanceof UserInterface && in_array($roles, $user->getRoles(), true)) {
                return;
            }
        }

        $explanation = 'User has not got the appropriate permission to publish the blog post';
        $event->addTransitionBlocker(new TransitionBlocker($explanation, '0', ['user' => $user]));

        $this->logger->alert(sprintf(
            'Guard blocked!!! when blog post (id: "%s") performed transition "%s" from "%s" to "%s"',
            $event->getSubject()->getId(),
            $event->getTransition() ? $event->getTransition()->getName() : null,
            implode(', ', array_keys($event->getMarking()->getPlaces())),
            implode(', ', ($event->getTransition() ? $event->getTransition()->getTos() : null))
        ));
    }

    public function guardReject(GuardEvent $event): void
    {
        $eventTransition = $event->getTransition();
        $roles = $event->getMetadata('roles', $eventTransition);

        $this->logger->info(sprintf(
            'Guard activated when blog post (id: "%s") performed transition "%s" from "%s" to "%s"',
            $event->getSubject()->getId(),
            $event->getTransition() ? $event->getTransition()->getName() : null,
            implode(', ', array_keys($event->getMarking()->getPlaces())),
            implode(', ', ($event->getTransition() ? $event->getTransition()->getTos() : null))
        ));

        $user = null;
        $token = $this->tokenStorage->getToken();
        if ($token instanceof TokenInterface) {
            $user = $token->getUser();
            if ($user instanceof UserInterface && in_array($roles, $user->getRoles(), true)) {
                return;
            }
        }

        $explanation = 'User has not got the appropriate permission to publish the blog post';
        $event->addTransitionBlocker(new TransitionBlocker($explanation, '0', ['user' => $user]));

        $this->logger->alert(sprintf(
            'Guard blocked!!! when blog post (id: "%s") performed transition "%s" from "%s" to "%s"',
            $event->getSubject()->getId(),
            $event->getTransition() ? $event->getTransition()->getName() : null,
            implode(', ', array_keys($event->getMarking()->getPlaces())),
            implode(', ', ($event->getTransition() ? $event->getTransition()->getTos() : null))
        ));
    }

    public static function getSubscribedEvents(): array
    {
        return [
            'workflow.blog_publishing.guard.' . BlogPostTransitions::PUBLISH => ['guardPublish'],
            'workflow.blog_publishing.guard.' . BlogPostTransitions::REJECT => ['guardReject'],
        ];
    }
}
