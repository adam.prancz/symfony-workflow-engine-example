<?php

namespace App\Workflow\Places;

/** Statuses */
final class BlogPostPlaces
{
    public const DRAFT = 'draft';
    public const REVIEWED = 'reviewed';
    public const PUBLISHED = 'published';
    public const REJECTED = 'rejected';
}
