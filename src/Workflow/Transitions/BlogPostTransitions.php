<?php

namespace App\Workflow\Transitions;

/** Statuses */
final class BlogPostTransitions
{
    public const DRAFT = 'to_draft';
    public const REVIEW = 'to_review';
    public const PUBLISH = 'publish';
    public const REJECT = 'reject';
}
