<?php

namespace App\Tests\Integration;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class HealthCheckTest extends WebTestCase
{
    public function testHealthCheck(): void
    {
        $client = static::createClient();
        $client->request('GET', '/health-check/ping');

        self::assertEquals(200, $client->getResponse()->getStatusCode());
        self::assertEquals('OK', $client->getResponse()->getContent());
    }
}
