<?php

namespace App\Tests\Repository;

use App\Entity\BlogPost;
use App\Repository\BlogPostRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Workflow\WorkflowInterface;

class BlogPostRepositoryTest extends WebTestCase
{
    protected ?BlogPostRepository  $repository = null;

    protected ?WorkflowInterface $workflow = null;

    /**
     * @testdox Test BlogPostRepository
     */
    public function testBlogPostRepository(): void
    {
        self::assertNotNull($this->repository);
    }

    /**
     * @testdox Test getting blog posts by given place (getByPlaces method of the repository)
     */
    public function testGetByPlaces(): void
    {
        $places = array_keys($this->workflow->getDefinition()->getPlaces());

        foreach ($places as $place) {
            $blogPosts = $this->repository->getByPlaces($place);

            if (!empty($blogPosts)) {
                foreach ($blogPosts as $blogPost) {
                    self::assertInstanceOf(BlogPost::class, $blogPost);
                    self::assertEquals($place, $blogPost->getCurrentPlace());
                }
            }
        }
    }

    protected function setUp(): void
    {
        parent::setUp();

        self::bootKernel();

        $container = self::$kernel->getContainer();

        $entityManager = $container->get('doctrine')->getManager();

        self::assertInstanceOf(EntityManager::class, $entityManager);

        $repository = $entityManager->getRepository(BlogPost::class);

        self::assertInstanceOf(BlogPostRepository::class, $repository);

        $this->repository = $repository;

        $workflow = $container->get('state_machine.blog_publishing');

        $this->workflow = $workflow;
    }
}
