<?php

namespace App\Tests\Workflow;

use App\Entity\BlogPost;
use App\Workflow\Places\BlogPostPlaces;
use App\Workflow\Transitions\BlogPostTransitions;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Workflow\WorkflowInterface;

class BlogPublishingLoggingTest extends WebTestCase
{
    protected ?WorkflowInterface $workflow = null;

    protected ?LoggerInterface $logger = null;

    protected function setUp(): void
    {
        parent::setUp();

        self::bootKernel();

        $container = self::$container;

        $workflow = $container->get('state_machine.blog_publishing');

        $this->workflow = $workflow;

        self::assertInstanceOf(WorkflowInterface::class, $workflow);
        self::assertEquals('blog_publishing', $this->workflow->getName());

        $logger = $container->get('monolog.logger');

        self::assertInstanceOf(LoggerInterface::class, $logger);

        $this->logger = $logger;
    }

    /**
     * @testdox Test logging of blog post transitions
     */
    public function testBlogPostTransitionsLogging(): void
    {
        $post = $this->getBlogPost();
        $this->workflow->apply($post, BlogPostTransitions::REVIEW);

        self::markTestIncomplete();
    }

    private function getBlogPost(?int $id = null): BlogPost
    {
        return (new BlogPost())
            ->setId($id ?: 1)
            ->setTitle('Test'.$id)
            ->setCurrentPlace(BlogPostPlaces::DRAFT);
    }
}
