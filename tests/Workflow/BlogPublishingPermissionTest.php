<?php

namespace App\Tests\Workflow;

use App\DataFixtures\UserFixtures;
use App\Entity\BlogPost;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Workflow\Places\BlogPostPlaces;
use App\Workflow\Transitions\BlogPostTransitions;
use Doctrine\ORM\NonUniqueResultException;
use Exception;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Guard\Token\PostAuthenticationGuardToken;
use Symfony\Component\Workflow\Exception\NotEnabledTransitionException;
use Symfony\Component\Workflow\TransitionBlocker;
use Symfony\Component\Workflow\WorkflowInterface;

class BlogPublishingPermissionTest extends WebTestCase
{
    protected ?WorkflowInterface $workflow = null;

    protected ?UserRepository $userRepository = null;

    protected ?KernelBrowser $client = null;

    protected function setUp(): void
    {
        parent::setUp();

        $this->client = static::createClient();

        $this->userRepository = static::$container->get(UserRepository::class);

        $container = self::$kernel->getContainer();

        $workflow = $container->get('state_machine.blog_publishing');

        $this->workflow = $workflow;

        self::assertInstanceOf(WorkflowInterface::class, $workflow);
        self::assertEquals('blog_publishing', $this->workflow->getName());
    }

    /**
     * @testdox Test permission checking by blog post publishing by admin user
     *
     * @throws NonUniqueResultException
     */
    public function testBlogPostPublishTransitionsPermissionAdmin(): void
    {
        $user = $this->userRepository->findOneByEmail(UserFixtures::ADMIN_USER_EMAIL);

        $tokenStorage = $this
            ->getMockBuilder(TokenStorageInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $tokenStorage
            ->method('getToken')
            ->willReturn(new PostAuthenticationGuardToken($user, 'api', []));

        $container = self::$container;
        $container->set('security.token_storage', $tokenStorage);

        $request = new Request();
        $request->setRequestFormat('json');

        $requestStack = $container->get('request_stack');
        $requestStack->push($request);

        $post = $this->getBlogPost(10);

        self::assertTrue($this->workflow->can($post, BlogPostTransitions::PUBLISH));

        $this->workflow->apply($post, BlogPostTransitions::PUBLISH);

        self::assertEquals(BlogPostPlaces::PUBLISHED, $post->getCurrentPlace());
    }

    /**
     * @testdox Test permission checking by blog post publishing by normal user
     *
     * @throws NonUniqueResultException
     * @throws Exception
     */
    public function testBlogPostPublishTransitionsPermissionUser(): void
    {
        $user = $this->userRepository->findOneByEmail(UserFixtures::USER_EMAIL);

        $tokenStorage = $this
            ->getMockBuilder(TokenStorageInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $tokenStorage
            ->method('getToken')
            ->willReturn(new PostAuthenticationGuardToken($user, 'api', []));

        $container = self::$container;
        $container->set('security.token_storage', $tokenStorage);

        $request = new Request();
        $request->setRequestFormat('json');

        $requestStack = $container->get('request_stack');
        $requestStack->push($request);

        $post = $this->getBlogPost(10);

        self::assertFalse($this->workflow->can($post, BlogPostTransitions::PUBLISH));

        /** @var TransitionBlocker $transitionBlocker */
        $transitionBlocker = $this->workflow->buildTransitionBlockerList($post, BlogPostTransitions::PUBLISH)->getIterator()->current();
        self::assertEquals('User has not got the appropriate permission to publish the blog post', $transitionBlocker->getMessage());
        self::assertInstanceOf(User::class, $transitionBlocker->getParameters()['user']);

        $this->expectException(NotEnabledTransitionException::class);
        $this->workflow->apply($post, BlogPostTransitions::PUBLISH);
    }

    /**
     * @testdox Test permission checking by blog post rejecting by admin user
     *
     * @throws NonUniqueResultException
     */
    public function testBlogPostRejectTransitionsPermissionAdmin(): void
    {
        $user = $this->userRepository->findOneByEmail(UserFixtures::ADMIN_USER_EMAIL);

        $tokenStorage = $this
            ->getMockBuilder(TokenStorageInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $tokenStorage
            ->method('getToken')
            ->willReturn(new PostAuthenticationGuardToken($user, 'api', []));

        $container = self::$container;
        $container->set('security.token_storage', $tokenStorage);

        $request = new Request();
        $request->setRequestFormat('json');

        $requestStack = $container->get('request_stack');
        $requestStack->push($request);

        $post = $this->getBlogPost(10);

        self::assertTrue($this->workflow->can($post, BlogPostTransitions::REJECT));

        $this->workflow->apply($post, BlogPostTransitions::REJECT);

        self::assertEquals(BlogPostPlaces::REJECTED, $post->getCurrentPlace());
    }

    /**
     * @testdox Test permission checking by blog post rejecting by normal user
     *
     * @throws NonUniqueResultException
     * @throws Exception
     */
    public function testBlogPostRejectTransitionsPermissionUser(): void
    {
        $user = $this->userRepository->findOneByEmail(UserFixtures::USER_EMAIL);

        $tokenStorage = $this
            ->getMockBuilder(TokenStorageInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $tokenStorage
            ->method('getToken')
            ->willReturn(new PostAuthenticationGuardToken($user, 'api', []));

        $container = self::$container;
        $container->set('security.token_storage', $tokenStorage);

        $request = new Request();
        $request->setRequestFormat('json');

        $requestStack = $container->get('request_stack');
        $requestStack->push($request);

        $post = $this->getBlogPost(10);

        self::assertFalse($this->workflow->can($post, BlogPostTransitions::PUBLISH));

        /** @var TransitionBlocker $transitionBlocker */
        $transitionBlocker = $this->workflow->buildTransitionBlockerList($post, BlogPostTransitions::PUBLISH)->getIterator()->current();
        self::assertEquals('User has not got the appropriate permission to publish the blog post', $transitionBlocker->getMessage());
        self::assertInstanceOf(User::class, $transitionBlocker->getParameters()['user']);

        $this->expectException(NotEnabledTransitionException::class);
        $this->workflow->apply($post, BlogPostTransitions::REJECT);
    }

    private function getBlogPost(?int $id = null): BlogPost
    {
        return (new BlogPost())
            ->setId($id ?: 1)
            ->setTitle('Test' . $id)
            ->setCurrentPlace(BlogPostPlaces::REVIEWED);
    }
}
