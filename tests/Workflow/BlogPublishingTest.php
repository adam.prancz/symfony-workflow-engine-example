<?php

namespace App\Tests\Workflow;

use App\DataFixtures\UserFixtures;
use App\Entity\BlogPost;
use App\Repository\UserRepository;
use App\Workflow\Places\BlogPostPlaces;
use App\Workflow\Transitions\BlogPostTransitions;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Guard\Token\PostAuthenticationGuardToken;
use Symfony\Component\Workflow\Exception\NotEnabledTransitionException;
use Symfony\Component\Workflow\WorkflowInterface;

class BlogPublishingTest extends WebTestCase
{
    protected ?WorkflowInterface $workflow = null;

    protected function setUp(): void
    {
        parent::setUp();

        self::bootKernel();

        $userRepository = static::$container->get(UserRepository::class);

        $user = $userRepository->findOneByEmail(UserFixtures::ADMIN_USER_EMAIL);

        $tokenStorage = $this
            ->getMockBuilder(TokenStorageInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $tokenStorage
            ->method('getToken')
            ->willReturn(new PostAuthenticationGuardToken($user, 'api', []));

        $container = self::$container;
        $container->set('security.token_storage', $tokenStorage);

        $request = new Request();
        $request->setRequestFormat('json');

        $requestStack = $container->get('request_stack');
        $requestStack->push($request);

        $workflow = $container->get('state_machine.blog_publishing');

        $this->workflow = $workflow;

        self::assertInstanceOf(WorkflowInterface::class, $workflow);
        self::assertEquals('blog_publishing', $this->workflow->getName());
    }

    /**
     * @testdox Test publishing blog post
     */
    public function testBlogPublishingTransitionsToPublish(): void
    {
        $post = $this->getBlogPost();

        self::assertEquals(BlogPostPlaces::DRAFT, $post->getCurrentPlace());

        self::assertNotEmpty($this->workflow->getEnabledTransitions($post));

        self::assertTrue($this->workflow->can($post, BlogPostTransitions::REVIEW));
        self::assertFalse($this->workflow->can($post, BlogPostTransitions::REJECT));
        self::assertFalse($this->workflow->can($post, BlogPostTransitions::PUBLISH));
        self::assertFalse($this->workflow->can($post, BlogPostTransitions::REJECT));

        // Update the currentState on the post
        $this->workflow->apply($post, BlogPostTransitions::REVIEW);

        self::assertEquals(BlogPostPlaces::REVIEWED, $post->getCurrentPlace());

        self::assertFalse($this->workflow->can($post, BlogPostTransitions::REVIEW));
        self::assertTrue($this->workflow->can($post, BlogPostTransitions::REJECT));
        self::assertTrue($this->workflow->can($post, BlogPostTransitions::PUBLISH));
        self::assertTrue($this->workflow->can($post, BlogPostTransitions::REJECT));

        $this->workflow->apply($post, BlogPostTransitions::PUBLISH);

        self::assertEquals(BlogPostPlaces::PUBLISHED, $post->getCurrentPlace());

        self::assertFalse($this->workflow->can($post, BlogPostTransitions::REVIEW));
        self::assertFalse($this->workflow->can($post, BlogPostTransitions::REJECT));
        self::assertFalse($this->workflow->can($post, BlogPostTransitions::PUBLISH));
        self::assertFalse($this->workflow->can($post, BlogPostTransitions::REJECT));

        self::assertEmpty($this->workflow->getEnabledTransitions($post));
    }

    /**
     * @testdox Test put back to draft
     */
    public function testBlogPublishingTransitionToDraft(): void
    {
        $post = $this->getBlogPost();

        self::assertEquals(BlogPostPlaces::DRAFT, $post->getCurrentPlace());

        self::assertNotEmpty($this->workflow->getEnabledTransitions($post));

        self::assertTrue($this->workflow->can($post, BlogPostTransitions::REVIEW));
        self::assertFalse($this->workflow->can($post, BlogPostTransitions::REJECT));
        self::assertFalse($this->workflow->can($post, BlogPostTransitions::PUBLISH));
        self::assertFalse($this->workflow->can($post, BlogPostTransitions::REJECT));

        // Update the currentState on the post
        $this->workflow->apply($post, BlogPostTransitions::REVIEW);

        self::assertEquals(BlogPostPlaces::REVIEWED, $post->getCurrentPlace());

        self::assertFalse($this->workflow->can($post, BlogPostTransitions::REVIEW));
        self::assertTrue($this->workflow->can($post, BlogPostTransitions::REJECT));
        self::assertTrue($this->workflow->can($post, BlogPostTransitions::PUBLISH));
        self::assertTrue($this->workflow->can($post, BlogPostTransitions::REJECT));

        $this->workflow->apply($post, BlogPostTransitions::DRAFT);

        self::assertEquals(BlogPostPlaces::DRAFT, $post->getCurrentPlace());

        self::assertTrue($this->workflow->can($post, BlogPostTransitions::REVIEW));
        self::assertFalse($this->workflow->can($post, BlogPostTransitions::DRAFT));
        self::assertFalse($this->workflow->can($post, BlogPostTransitions::PUBLISH));
        self::assertFalse($this->workflow->can($post, BlogPostTransitions::REJECT));

        self::assertNotEmpty($this->workflow->getEnabledTransitions($post));
    }

    /**
     * @testdox Test rejecting blog post
     */
    public function testBlogPublishingTransitionToRejected(): void
    {
        $post = $this->getBlogPost();

        self::assertEquals(BlogPostPlaces::DRAFT, $post->getCurrentPlace());

        self::assertNotEmpty($this->workflow->getEnabledTransitions($post));

        self::assertTrue($this->workflow->can($post, BlogPostTransitions::REVIEW));
        self::assertFalse($this->workflow->can($post, BlogPostTransitions::REJECT));
        self::assertFalse($this->workflow->can($post, BlogPostTransitions::PUBLISH));
        self::assertFalse($this->workflow->can($post, BlogPostTransitions::REJECT));

        // Update the currentState on the post
        $this->workflow->apply($post, BlogPostTransitions::REVIEW);

        self::assertEquals(BlogPostPlaces::REVIEWED, $post->getCurrentPlace());

        self::assertFalse($this->workflow->can($post, BlogPostTransitions::REVIEW));
        self::assertTrue($this->workflow->can($post, BlogPostTransitions::REJECT));
        self::assertTrue($this->workflow->can($post, BlogPostTransitions::PUBLISH));
        self::assertTrue($this->workflow->can($post, BlogPostTransitions::REJECT));

        $this->workflow->apply($post, BlogPostTransitions::REJECT);

        self::assertEquals(BlogPostPlaces::REJECTED, $post->getCurrentPlace());

        self::assertFalse($this->workflow->can($post, BlogPostTransitions::REVIEW));
        self::assertFalse($this->workflow->can($post, BlogPostTransitions::REJECT));
        self::assertFalse($this->workflow->can($post, BlogPostTransitions::PUBLISH));
        self::assertFalse($this->workflow->can($post, BlogPostTransitions::REJECT));

        self::assertEmpty($this->workflow->getEnabledTransitions($post));
    }

    /**
     * @testdox Test throw exception for invalid transition
     */
    public function testBlogPublishingTransitionsNotAllowed(): void
    {
        $post = $this->getBlogPost();

        $this->expectException(NotEnabledTransitionException::class);
        $this->workflow->apply($post, BlogPostTransitions::REJECT);
    }

    /**
     * @testdox Test getting place of a given blog post
     */
    public function testBlogPostListings(): void
    {
        $post = $this->getBlogPost();
        $post1 = $this->getBlogPost(1);
        $post2 = $this->getBlogPost(2);

        self::assertEquals(BlogPostPlaces::DRAFT, $post->getCurrentPlace());
        self::assertEquals(BlogPostPlaces::DRAFT, $post1->getCurrentPlace());
        self::assertEquals(BlogPostPlaces::DRAFT, $post2->getCurrentPlace());
    }

    private function getBlogPost(?int $id = null): BlogPost
    {
        return (new BlogPost())
            ->setId($id ?: 1)
            ->setTitle('Test'.$id)
            ->setCurrentPlace(BlogPostPlaces::DRAFT);
    }
}
